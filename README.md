# Pipelines with Laravel

Pipelines is a plugin for Bitbucket. The purpose of Pipelines is to enable developers to use [CI/CD](https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd). This will enable developers to have tests executed typically on any git push to Bitbucket. This gives developers a sense of confidence, ensuring that their commit(s) do not introduce new problems to the codebase. This is, however, not a guarantee that there will not be any problems.

This is an example repo showing [Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/build-test-and-deploy-with-pipelines-792496469.html) usage with Laravel. Other examples exist in other language below.

* Pipeline Example Repos:
  * [Java](https://bitbucket.org/bootcamp-pipelines/pipelines-java/src/master/)

  * [Laravel](https://bitbucket.org/bootcamp-pipelines/pipelines-laravel/src/master/)

  * [Node](https://bitbucket.org/bootcamp-pipelines/pipelines-node/src/master/)

  * [PHP](https://bitbucket.org/bootcamp-pipelines/pipelines-php/src/master/)

  * [Ruby](https://bitbucket.org/bootcamp-pipelines/pipelines-ruby/src/master/)

[Laravel Bitbucket Pipelines Reference Page](https://confluence.atlassian.com/bitbucket/laravel-with-bitbucket-pipelines-913473967.html)


## Setup

1. Fork this repository by clicking the plus sign and then "Fork this repository"

![Fork Repository Tutorial](https://media.giphy.com/media/35MvfIa8XbwnDRD0g5/giphy.gif)

2. On the left navigation bar, click on "Pipelines" Scroll down to view the bitbucket-pipelines.yml file and click "Enable"

![Enable Pipelines Tutorial](https://media.giphy.com/media/3JTpamHn2MX0RrO7SJ/giphy.gif)

3. Watch your build run

![Build Progress](https://media.giphy.com/media/82l6PwEc47oTtkfh7i/giphy.gif)

NOTE: Bitbucket Pipelines includes fifty free minutes per repo, at the time of writing. The remaining minutes can be checked by clicking on the "Usage" button on the top right under in the "Pipelines" tab.


## Basic Commands

File: `bitbucket-pipelines.yml`


```
image: php:7-fpm

pipelines:
  default:order
    - step:
        script:
          - apt-get update && apt-get install -qy git curl libmcrypt-dev mysql-client
          - yes | pecl install mcrypt-1.0.1
          - docker-php-ext-install pdo_mysql
          - bash ./install-composer.sh
          - composer install
          - ln -f -s .env.pipelines .env
          - php artisan migrate
          - php artisan serve &
          - sleep 5
          - ./vendor/bin/phpunit
          - curl -vk http://localhost:8000
```

For in-depth configuration information, visit the Bitbucket Pipelines [YAML Configuration Page](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html). A list of the more useful keywords at listed below.

`step`: Each step loads a new Docker container that includes a clone of the current repository. The contents in each script keyword is then executed sequentially.

`script`: A list of commands that are executed sequentially.

`caches`: Re-downloading dependencies from the internet for each step of a build can take a lot of time. The cache can be specified to store content so it can be reused upon each build.

## Advanced Commands

Connecting to a database is also quite simple, by adding a few lines to `bitbucket-pipelines.yml`. This will specify and define the database.

The resulting file may looks like this:

```
image: php:7-fpm

pipelines:
  default:order
    - step:
        script:
          - apt-get update && apt-get install -qy git curl libmcrypt-dev mysql-client
          - yes | pecl install mcrypt-1.0.1
          - docker-php-ext-install pdo_mysql
          - bash ./install-composer.sh
          - composer install
          - ln -f -s .env.pipelines .env
          - php artisan migrate
          - php artisan serve &
          - sleep 5
          - ./vendor/bin/phpunit
          - curl -vk http://localhost:8000
        services:
          - mysql
definitions:
  services:
    mysql:
      image: mysql:5.7
      environment:
        MYSQL_DATABASE: 'homestead'
        MYSQL_RANDOM_ROOT_PASSWORD: 'yes'
        MYSQL_USER: 'homestead'
        MYSQL_PASSWORD: 'secret'
```
The mysql enviroment variables should be the same as defined in the .env.pipelines file.

## Running the repo locally

    cd quickstart

    composer install

    php artisan migrate

    php artisan serve

[Complete Tutorial](https://laravel.com/docs/5.2/quickstart)